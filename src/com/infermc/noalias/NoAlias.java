package com.infermc.noalias;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class NoAlias extends JavaPlugin implements Listener {

    private Boolean whitelist = true;
    private ArrayList<String> pluginList = new ArrayList<String>();
    private String disallowed = "";

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this,this);
        saveDefaultConfig();
        loadList();
    }

    @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent ev) {
        if (ev.isCancelled()) return;
        if (ev.getPlayer().isOp()) return;

        String cmd = ev.getMessage().split(" ")[0].substring(1);
        String[] temp = cmd.split(":");
        if (temp.length >= 2) {
            String plugin = temp[0].toLowerCase();
            if (whitelist) {
                if (pluginList.contains(plugin)) {
                    return;
                } else {
                    if (!disallowed.equals("")) ev.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',disallowed));
                    ev.setCancelled(true);
                }
            } else {
                if (pluginList.contains(plugin)) {
                    if (!disallowed.equals("")) ev.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',disallowed));
                    ev.setCancelled(true);
                } else {
                    return;
                }
            }
        }
    }
    @EventHandler
    public void onCMDTAB(TabCompleteEvent ev) {
        if (ev.isCancelled()) return;
        if (ev.getSender().isOp()) return;

        String cmd = ev.getBuffer().split(" ")[0].substring(1);
        String[] temp = cmd.split(":");
        if (temp.length >= 2) {
            String plugin = temp[0].toLowerCase();
            if (whitelist) {
                if (pluginList.contains(plugin)) {
                    return;
                } else {
                    ev.setCancelled(true);
                }
            } else {
                if (pluginList.contains(plugin)) {
                    ev.setCancelled(true);
                } else {
                    return;
                }
            }
        }
    }


    public void loadList() {
        String type = getConfig().getString("type","whitelist");
        ArrayList<String> list = (ArrayList<String>) getConfig().getList("plugins",new ArrayList<String>());
        disallowed = getConfig().getString("message","");

        pluginList.clear();
        for (String l : list) {
            pluginList.add(l.toLowerCase());
        }
        if (type.equalsIgnoreCase("whitelist")) {
            whitelist = true;
        } else if (type.equalsIgnoreCase("blacklist")) {
            whitelist = false;
        }
    }
}
